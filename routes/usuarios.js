var express = require('express');
var router = express.Router();
const usuariosController = require('../controllers/usuarios');



/* GET home page. */
router.post('/', function(req, res) {

    const nombres = req.body.nombres;
    const apellidos = req.body.apellidos;
    const email = req.body.email;
    const telefono = req.body.telefono;
    const password = req.body.password;

    console.log(req.body);

    usuariosController.store(nombres, apellidos, email, telefono, password).then(
        (success) => {
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

router.get('/', function(req, res) {

    usuariosController.list().then(
        (success) => {
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

router.get('/:usuario_id/show', function(req, res, next) {
    usuariosController.show(req.params.usuario_id).then(
        (success) => {
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});


router.delete('/:usuario_id', function(req, res, next) {
    usuariosController.delete(req.params.usuario_id).then(
        (success) => {
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

router.put('/', function(req, res, next) {
    const usuario_id = req.body.usuario_id;
    const nombres = req.body.nombres;
    const apellidos = req.body.apellidos;
    const email = req.body.email;
    const direccion = req.body.direccion;
    const telefono = req.body.telefono;
    const password = req.body.password;

    usuariosController.update(usuario_id, nombres, apellidos, email, direccion, telefono, password).then(
        (success) => {
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

module.exports = router;
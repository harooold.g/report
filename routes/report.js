var express = require('express');
var router = express.Router();
const reportController = require('../controllers/report');
var path = require('path');

router.get('/html', function (req, res, next) {
  res.sendFile('./public/index.html', { root: '.' });
});

router.get('/screen', function (req, res, next) {
  puppeteer.launch().then(async function () {
    const htmlFile = path.resolve('./public/index.html');
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://localhost:3000/report/html?objeto=MAQ02', {
      waitUntil: 'networkidle2',
    });

    await page.pdf({
      path: './public/excel/reporte.pdf',
      format: 'Letter',
    });
    await browser.close();
  });
});

router.get('/objeto/:hub_id', function (req, res, next) {
  reportController.reportObjeto(req.params.hub_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/data/:objeto_id', function (req, res, next) {
  const fecha = new Date();
  let fechamax = new Date();
  let fechamin = fecha.setDate(fecha.getDate() - 1);
  reportController.reportData(req.params.objeto_id, fechamin, fechamax).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

module.exports = router;

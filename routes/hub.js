var express = require('express');
var router = express.Router();
const hubController = require('../controllers/hub');

/* GET home page. */
router.post('/', function (req, res) {
  const nombre = req.body.nombre;
  const empresa = req.body.empresa;
  const descripcion = req.body.descripcion;

  console.log(req.body);

  hubController.store(nombre, empresa, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/', function (req, res) {
  hubController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/:hub_id/show', function (req, res, next) {
  hubController.show(req.params.hub_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete('/:hub_id', function (req, res, next) {
  hubController.delete(req.params.hub_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put('/', function (req, res, next) {
  const hub_id = req.body.hub_id;
  const nombre = req.body.nombre;
  const empresa = req.body.empresa;
  const descripcion = req.body.descripcion;

  hubController.update(hub_id, nombre, empresa, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

module.exports = router;

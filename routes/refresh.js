var express = require('express');
var router = express.Router();
const authController = require('../controllers/auth');



const generate = (rToken, data) => {
    console.log("aaa");
    return new Promise((resolve, reject) => {
        authController.refresh(data).then(
            (result) => {
                console.log(result);
                refreshTokens = refreshTokens.filter(token => token !== rToken)
                resolve(result);
            },
            (error) => {
                console.log(error);
                reject({
                    "success": error.success,
                    "message": error.message
                });
            }
        )
    });
}

/* GET home page. */
router.post('/', function(req, res, next) {
    const rToken = req.cookies.rftk || '';
    if (!rToken) return res.sendStatus(401);
    utils.validateRefreshToken(rToken).then(
        (data) => {
            delete data.iat;
            delete data.exp;
            generate(rToken, data).then(
                (result) => {
                    res.cookie('rftk', result.refreshToken, {
                        secure: false,
                        httpOnly: true,
                        expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000),
                    })

                    refreshTokens.push(result.refreshToken);
                    delete result.refreshToken;

                    res.json(result);
                },
                (error) => {
                    res.json(error);
                }
            );
        },
        (err) => {
            res.sendStatus(401);
        }
    )
});

router.post('/app', function(req, res) {
    const rToken = req.body.rftk || '';
    if (!rToken) return res.sendStatus(401);
    utils.validateRefreshToken(rToken).then(
        (data) => {
            delete data.iat;
            delete data.exp;
            generate(rToken, data).then(
                (result) => {
                    refreshTokens.push(result.refreshToken);
                    res.json(result)
                },
                (error) => {
                    res.sendStatus(401);
                }
            );
        },
        (err) => {
            res.sendStatus(401);
        }
    )
});

module.exports = router;
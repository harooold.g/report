var express = require('express');
var router = express.Router();
const objetoController = require('../controllers/objeto');

/* GET home page. */
router.post('/', function (req, res) {
  const hub_id = req.body.hub_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;

  console.log(req.body);

  objetoController.store(hub_id, nombre, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/', function (req, res) {
  objetoController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/:objeto_id/show', function (req, res, next) {
  objetoController.show(req.params.objeto_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/hubs/:hub_id', function (req, res) {
  objetoController.listByHubs(req.params.hub_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete('/:objeto_id', function (req, res, next) {
  objetoController.delete(req.params.objeto_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put('/', function (req, res, next) {
  const objeto_id = req.body.objeto_id;
  const hub_id = req.body.hub_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;

  objetoController.update(objeto_id, hub_id, nombre, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

module.exports = router;

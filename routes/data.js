var express = require('express');
var router = express.Router();
const dataController = require('../controllers/data');

/* GET home page. */
router.post('/', function (req, res) {
  const objeto_id = req.body.objeto_id;
  const dato1 = req.body.dato1;
  const dato2 = req.body.dato2;
  const fecha_registro = new Date();

  console.log(req.body);

  dataController.store(objeto_id, dato1, dato2, fecha_registro).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/', function (req, res) {
  dataController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/today', function (req, res) {
  const fecha = new Date();
  const fechamax = new Date();
  const fechamin = fecha.setDate(fecha.getDate() - 1);

  console.log(fechamax);
  console.log(fecha);
  dataController.listByFecha(fechamin, fechamax).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/fecha', function (req, res) {
  const fechamin = req.body.fechamin;
  const fechamax = req.body.fechamax;
  dataController.listByFecha(fechamin, fechamax).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/:data_id/show', function (req, res, next) {
  dataController.show(req.params.data_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.post('/objeto/:objeto_id', function (req, res) {
  console.log(req.body);
  const fecha = new Date();
  let fechamax = new Date();
  let fechamin = fecha.setDate(fecha.getDate() - 1);
  if (req.body.fechamax) {
    fechamax = req.body.fechamax;
    fechamin = req.body.fechamin;
  }
  dataController.listByObjetos(req.params.objeto_id, fechamin, fechamax).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete('/:data_id', function (req, res, next) {
  dataController.delete(req.params.data_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put('/', function (req, res, next) {
  const data_id = req.body.data_id;
  const objeto_id = req.body.objeto_id;
  const dato1 = req.body.dato1;
  const dato2 = req.body.dato2;

  dataController.update(data_id, objeto_id, dato1, dato2).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

module.exports = router;

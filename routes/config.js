var express = require('express');
var router = express.Router();
const configController = require('../controllers/config');

/* GET home page. */
router.post('/', function (req, res) {
  const correo1 = req.body.correo1;
  const correo2 = req.body.correo2;
  const correo3 = req.body.correo3;
  const correo4 = req.body.correo2;
  const correo5 = req.body.correo3;

  const asunto = req.body.asunto;
  const contenido = req.body.contenido;
  const time = req.body.time;
  console.log(req.body);

  configController
    .store(time, correo1, correo2, correo3, correo4, correo5, asunto, contenido)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get('/', function (req, res) {
  configController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get('/:config_id/show', function (req, res, next) {
  configController.show(req.params.config_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete('/:config_id', function (req, res, next) {
  configController.delete(req.params.config_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put('/', function (req, res, next) {
  const config_id = req.body._id;
  const correo1 = req.body.correo1;
  const correo2 = req.body.correo2;
  const correo3 = req.body.correo3;
  const correo4 = req.body.correo2;
  const correo5 = req.body.correo3;
  const asunto = req.body.asunto;
  const contenido = req.body.contenido;
  console.log(req.body);

  configController
    .update(
      config_id,
      correo1,
      correo2,
      correo3,
      correo4,
      correo5,
      asunto,
      contenido
    )
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

module.exports = router;

const usuariosModels = require('../models/usuarios');

const auth = {

    login(email, password) {
        return new Promise((resolve, reject) => {
            usuariosModels.findOne({ email: email, password: utils.encryp(password) }).then(
                (rowUser) => {
                    if (!rowUser) reject({ message: 'Usuario no existe' });
                    resolve(rowUser);
                },
                (error) => {
                    console.log("error");
                    reject(error);
                }
            )
        });
    },

    afterlogin(email) {
        return new Promise((resolve, reject) => {
            usuariosModels.findOne({ email: email }, { password: false, estado: false, fecha_registro: false, sockets: false }).then(
                (rowUser) => {
                    if (!rowUser) reject({ message: 'Usuario no existe' });
                    const token = utils.generateAccessToken(rowUser.toJSON());
                    const rtoken = utils.generateRefreshToken(rowUser.toJSON());
                    resolve({
                        token: token,
                        refreshToken: rtoken,
                        _id: rowUser._id
                    });
                },
                (error) => {
                    reject(error)
                }
            )
        });
    },

    refresh(data) {
        return new Promise((resolve, reject) => {
            console.log("bbb");
            const token = utils.generateAccessToken(data);
            console.log("ccc");
            const rtoken = utils.generateRefreshToken(data);
            resolve({
                token: token,
                refreshToken: rtoken,
                _id: data._id
            });
        });

    }
}

module.exports = auth;
const configModels = require('../models/config');

const config = {
  show(config_id) {
    return new Promise((resolve, reject) => {
      configModels.findOne({ _id: config_id }).then(
        (rowAll) => {
          if (!rowAll) reject({ message: 'objeto no existe' });
          resolve(rowAll);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(time, correo1, correo2, correo3, correo4, correo5, asunto, contenido) {
    return new Promise((resolve, reject) => {
      configModels
        .create({
          time,
          correo1,
          correo2,
          correo3,
          correo4,
          correo5,
          asunto,
          contenido,
        })
        .then(
          (newconfig) => {
            console.log(newconfig);
            resolve(newconfig);
          },
          (error) => {
            console.log(error);
            reject(error);
          }
        );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      configModels.find().then(
        (listconfig) => {
          resolve(listconfig);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(config_id) {
    return new Promise((resolve, reject) => {
      configModels.deleteOne({ _id: config_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(
    config_id,
    correo1,
    correo2,
    correo3,
    correo4,
    correo5,
    asunto,
    contenido
  ) {
    return new Promise((resolve, reject) => {
      let _set = {
        correo1,
        correo2,
        correo3,
        correo4,
        correo5,
        asunto,
        contenido,
      };

      configModels.updateOne({ _id: config_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: 'config no existe' });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
};

module.exports = config;

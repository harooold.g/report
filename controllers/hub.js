const hubModels = require('../models/hub');

const hub = {
  show(hub_id) {
    return new Promise((resolve, reject) => {
      hubModels.finOne({ _id: hub_id }).then(
        (rowhub) => {
          if (!rowhub) reject({ message: 'hub no existe' });
          resolve(rowhub);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(nombre, empresa, descripcion) {
    return new Promise((resolve, reject) => {
      hubModels
        .create({
          nombre,
          empresa,
          descripcion,
        })
        .then(
          (newhub) => {
            console.log(newhub);
            resolve(newhub);
          },
          (error) => {
            console.log(error);
            reject(error);
          }
        );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      hubModels.find().then(
        (listhub) => {
          resolve(listhub);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(hub_id) {
    return new Promise((resolve, reject) => {
      hubModels.deleteOne({ _id: hub_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(hub_id, nombre, empresa, descripcion) {
    return new Promise((resolve, reject) => {
      let _set = {
        nombre,
        empresa,
        descripcion,
      };

      hubModels.updateOne({ _id: hub_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: 'hub no existe' });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
};

module.exports = hub;

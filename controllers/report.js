const dataModels = require('../models/data');
const objetoModels = require('../models/objeto');
const hubModels = require('../models/hub');
const configModels = require('../models/config');

const report = {
  reportObjeto(hub_id) {
    return new Promise((resolve, reject) => {
      objetoModels.find({ hub_id: hub_id }).then(
        (listObjeto) => {
          hubModels
            .populate(listObjeto, {
              path: 'hub_id',
              select: '_id nombre descripcion',
            })
            .then(
              (rowAll) => {
                if (!rowAll) reject({ message: 'hub no existe' });
                mkexcel2(rowAll);
                resolve(rowAll);
              },
              (error) => {
                reject(error);
              }
            );
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  reportData(objeto_id, fechamin, fechamax) {
    let id = objeto_id;
    return new Promise((resolve, reject) => {
      dataModels
        .find({
          objeto_id: objeto_id,
          fecha_registro: {
            $gte: fechamin,
            $lt: fechamax,
          },
        })
        .then(
          (listdata) => {
            objetoModels
              .populate(listdata, {
                path: 'objeto_id',
                select: '_id nombre descripcion',
              })
              .then(
                (rowAll) => {
                  mkpdf(id);
                  mkexcel(rowAll);
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },
};

async function mkexcel(data) {
  const workbook = new excel.Workbook();
  const worksheet = workbook.addWorksheet('My Sheet');

  worksheet.columns = [
    { header: 'OBJETO', key: 'objeto', width: 10 },
    { header: 'CORRIENTE', key: 'corriente', width: 32 },
    { header: 'TORQUE', key: 'torque', width: 15 },
    { header: 'FECHA', key: 'fecha', width: 15 },
  ];
  for (let i in data) {
    worksheet.addRow({
      objeto: data[i].objeto_id.nombre,
      corriente: data[i].dato1,
      torque: data[i].dato2,
      fecha: data[i].fecha_registro,
    });
  }
  await workbook.xlsx.writeFile('./public/excel/reporte.xlsx');

  console.log('File is written');
}

async function sendmail() {
  let listconfig = await list();
  console.log(listconfig);

  const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'tabitasrules@gmail.com',
      pass: 'Li4mpapi',
    },
  });

  const messageOptions = {
    subject: listconfig[0].asunto,
    text: listconfig[0].contenido,
    to: `${listconfig[0].correo1}, ${listconfig[0]?.correo2}, ${listconfig[0]?.correo3}`,
    from: 'tabitasrules@gmail.com',
    attachments: [
      {
        filename: 'reporte.xlsx',
        path: './public/excel/reporte.xlsx',
      },
      {
        filename: 'reporte.pdf',
        path: './public/excel/reporte.pdf',
      },
    ],
  };

  transporter.sendMail(messageOptions);
  /*let datafinal = [];
  for (i in data) {
    datafinal.push({
      objeto: data[i].objeto_id.nombre,
      corriente: data[i].dato1,
      torque: data[i].dato2,
      fecha: data[i].fecha_registro,
    });
  }*/
}

function list() {
  return new Promise((resolve, reject) => {
    configModels.find().then(
      (listconfig) => {
        resolve(listconfig);
      },
      (error) => {
        reject(error);
      }
    );
  });
}

function mkpdf(objeto_id) {
  console.log('PDF');
  console.log(objeto_id);
  puppeteer.launch().then(async function () {
    const browser = await puppeteer.launch({
      headless: false,
      args: ['--no-sandbox'],
    });
    const page = await browser.newPage();
    await page.goto(`http://localhost:3000/report/html?objeto=${objeto_id}`, {
      waitUntil: 'networkidle2',
    });

    await page.pdf({
      path: './public/excel/reporte.pdf',
      format: 'Letter',
    });
    await browser.close();
    console.log('PDF DONE!');
    sendmail();
  });
}

module.exports = report;

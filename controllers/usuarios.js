const usuariosModels = require('../models/usuarios');

const usuarios = {

    show(usuario_id) {
        return new Promise((resolve, reject) => {

            usuariosModels.finOne({ _id: usuario_id }, { nombres: true, apellidos: true, email: true, telefono: true, sockets: true }).then(
                (rowUsuario) => {
                    if (!rowUsuario) reject({ message: 'Usuario no existe' });
                    resolve(rowUsuario);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    store(nombres, apellidos, email, telefono, password) {
        //console.log(email);
        return new Promise((resolve, reject) => {
            usuariosModels.create({
                nombres,
                apellidos,
                email,
                telefono,
                password: utils.encryp(password)
            }).then(
                (newUsuario) => {
                    console.log(newUsuario);
                    resolve({
                        _id: newUsuario._id,
                        nombres: newUsuario.nombres,
                        apellidos: newUsuario.apellidos,
                        email: newUsuario.email,
                        telefono: newUsuario.telefono
                    })
                },
                (error) => {
                    console.log(error);
                    reject(error);
                }
            )
        });
    },

    list() {
        return new Promise((resolve, reject) => {
            usuariosModels.find({}, { _id: true, nombres: true, apellidos: true, email: true, telefono: true }).then(
                (listUsuario) => {
                    resolve(listUsuario);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    delete(usuario_id) {
        return new Promise((resolve, reject) => {
            usuariosModels.deleteOne({ _id: usuario_id }).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    update(usuario_id, nombres, apellidos, email, telefono, password) {
        return new Promise((resolve, reject) => {
            let _set = {
                nombres,
                apellidos,
                email,
                telefono
            }

            if (password && password != "undefined") _set.password = utils.encryp(password);

            usuariosModels.updateOne({ _id: usuario_id }, { $set: _set }).then(
                (result) => {
                    if (!result.n) reject({ message: 'Usuario no existe' })
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    login(email, password) {
        return new Promise((resolve, reject) => {
            usuariosModels.findOne({ email: email, password: utils.encryp(password) }, { password: false, estado: false, fecha_registro: false, sockets: false }).then(
                (rowUsuario) => {
                    if (!rowUsuario) reject({ message: 'Usuario no existe ' })
                    const token = utils.generateAccessToken(rowUsuario.toJSON());
                    const rtoken = utils.generateRefreshToken(rowUsuario.toJSON());

                    resolve({
                        _id: rowUsuario._id,
                        nombres: rowUsuario.nombres,
                        apellidos: rowUsuario.apellidos,
                        email: rowUsuario.email,
                        token: token,
                        refreshToken: rtoken
                    })
                },
                (error) => {
                    reject(error)
                }
            )
        });
    },

    updateSocket(user_id, socket) {
        return new Promise((resolve, reject) => {
            this.show(user_id).then(
                (rowUser) => {
                    let newSockets = rowUser.sockets;
                    newSockets.push(socket);

                    usuariosModels.updateOne({ _id: user_id }, { $set: { sockets: newSockets } }).then(
                        (result) => {
                            resolve(result);
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    disconectSocket(user_id, socket) {
        return new Promise((resolve, reject) => {
            this.show(user_id).then(
                (rowUser) => {
                    let newSockets = rowUser.sockets;
                    const index = newSockets.indexOf(socket.id);
                    newSockets.splice(index, 1);

                    usuariosModels.updateOne({ _id: user_id }, { $set: { sockets: newSockets } }).then(
                        (result) => {
                            resolve(result);
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                },
                (error) => {
                    reject(error);
                }
            )
        });
    }
}

module.exports = usuarios;
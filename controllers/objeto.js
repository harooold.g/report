const objetoModels = require('../models/objeto');
const hubModels = require('../models/hub');

const objeto = {
  show(objeto_id) {
    return new Promise((resolve, reject) => {
      objetoModels.finOne({ _id: objeto_id }).then(
        (rowobjeto) => {
          hubModels
            .populate(rowobjeto, {
              path: 'hub_id',
              select: '_id nombre descripcion',
            })
            .then(
              (rowAll) => {
                if (!rowAll) reject({ message: 'objeto no existe' });
                resolve(rowAll);
              },
              (error) => {
                reject(error);
              }
            );
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(hub_id, nombre, descripcion) {
    return new Promise((resolve, reject) => {
      objetoModels
        .create({
          hub_id,
          nombre,
          descripcion,
        })
        .then(
          (newobjeto) => {
            console.log(newobjeto);
            resolve(newobjeto);
          },
          (error) => {
            console.log(error);
            reject(error);
          }
        );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      objetoModels.find().then(
        (listobjeto) => {
          hubModels
            .populate(listobjeto, {
              path: 'hub_id',
              select: '_id nombre descripcion',
            })
            .then(
              (rowAll) => {
                if (!rowAll) reject({ message: 'objeto no existe' });
                resolve(rowAll);
              },
              (error) => {
                reject(error);
              }
            );
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(objeto_id) {
    return new Promise((resolve, reject) => {
      objetoModels.deleteOne({ _id: objeto_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(objeto_id, hub_id, nombre, descripcion) {
    return new Promise((resolve, reject) => {
      let _set = {
        hub_id,
        nombre,
        descripcion,
      };

      objetoModels.updateOne({ _id: objeto_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: 'objeto no existe' });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  listByHubs(hub_id) {
    return new Promise((resolve, reject) => {
      objetoModels
        .find(
          { hub_id: hub_id },
          {
            _id: true,
            hub_id: true,
            nombre: true,
            empresa: true,
            descripcion: true,
          }
        )
        .then(
          (listObjeto) => {
            hubModels
              .populate(listObjeto, {
                path: 'hub_id',
                select: '_id nombre descripcion',
              })
              .then(
                (rowAll) => {
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },
};

module.exports = objeto;

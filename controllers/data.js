const dataModels = require('../models/data');
const objetoModels = require('../models/objeto');

const data = {
  show(data_id) {
    return new Promise((resolve, reject) => {
      dataModels.findOne({ _id: data_id }).then(
        (rowdata) => {
          objetoModels
            .populate(rowdata, {
              path: 'objeto_id',
              select: '_id nombre descripcion',
            })
            .then(
              (rowAll) => {
                if (!rowAll) reject({ message: 'objeto no existe' });
                resolve(rowAll);
              },
              (error) => {
                reject(error);
              }
            );
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(objeto_id, dato1, dato2, fecha_registro) {
    return new Promise((resolve, reject) => {
      dataModels
        .create({
          objeto_id,
          dato1,
          dato2,
          fecha_registro,
        })
        .then(
          (newdata) => {
            console.log(newdata);
            resolve(newdata);
          },
          (error) => {
            console.log(error);
            reject(error);
          }
        );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      dataModels.find().then(
        (listdata) => {
          objetoModels
            .populate(listdata, {
              path: 'objeto_id',
              select: '_id nombre descripcion',
            })
            .then(
              (rowAll) => {
                if (!rowAll) reject({ message: 'objeto no existe' });
                resolve(rowAll);
              },
              (error) => {
                reject(error);
              }
            );
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(data_id) {
    return new Promise((resolve, reject) => {
      dataModels.deleteOne({ _id: data_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(data_id, objeto_id, dato1, dato2) {
    return new Promise((resolve, reject) => {
      let _set = {
        objeto_id,
        dato1,
        dato2,
      };

      dataModels.updateOne({ _id: data_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: 'data no existe' });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  listByObjetos(objeto_id, fechamin, fechamax) {
    return new Promise((resolve, reject) => {
      dataModels
        .find({
          objeto_id: objeto_id,
          fecha_registro: {
            $gte: fechamin,
            $lt: fechamax,
          },
        })
        .then(
          (listdata) => {
            objetoModels
              .populate(listdata, {
                path: 'objeto_id',
                select: '_id nombre descripcion',
              })
              .then(
                (rowAll) => {
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  listByFecha(FechaMin, FechaMax) {
    return new Promise((resolve, reject) => {
      dataModels
        .find({
          fecha_registro: {
            $gte: FechaMin,
            $lt: FechaMax,
          },
        })
        .then(
          (listdata) => {
            objetoModels
              .populate(listdata, {
                path: 'objeto_id',
                select: '_id nombre descripcion',
              })
              .then(
                (rowAll) => {
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  listByToday(FechaMin, FechaMax) {
    return new Promise((resolve, reject) => {
      dataModels
        .find({
          fecha_registro: {
            $gte: FechaMin,
            $lt: FechaMax,
          },
        })
        .then(
          (listdata) => {
            objetoModels
              .populate(listdata, {
                path: 'objeto_id',
                select: '_id nombre descripcion',
              })
              .then(
                (rowAll) => {
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },
};

module.exports = data;

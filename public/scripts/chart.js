const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const objeto = urlParams.get('objeto');
console.log(objeto);
const Http = new XMLHttpRequest();
const url = `http://18.224.184.127:3000/data/objeto/${objeto}`;

am4core.useTheme(am4themes_animated);

/*Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = dateAxis;
chart.cursor.snapToSeries = series;*/

var result = [];
Http.open('POST', url, true);
Http.send();

Http.onreadystatechange = (e) => {
  result = JSON.parse(Http.responseText);

  var chart = am4core.create('chartdiv1', am4charts.XYChart);

  chart.data = generateChartData1();

  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.minGridDistance = 50;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

  // Create series
  var series = chart.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = 'visits';
  series.dataFields.dateX = 'date';
  series.strokeWidth = 2;
  series.minBulletDistance = 10;
  series.tooltipText = '{valueY}';
  series.tooltip.pointerOrientation = 'vertical';
  series.tooltip.background.cornerRadius = 20;
  series.tooltip.background.fillOpacity = 0.5;
  series.tooltip.label.padding(12, 12, 12, 12);

  //GRAFICA 2
  var chart2 = am4core.create('chartdiv2', am4charts.XYChart);

  chart2.data = generateChartData2();

  // Create axes
  var dateAxis = chart2.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.minGridDistance = 50;

  var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());

  // Create series
  var series = chart2.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = 'visits';
  series.dataFields.dateX = 'date';
  series.strokeWidth = 2;
  series.minBulletDistance = 10;
  series.tooltipText = '{valueY}';
  series.tooltip.pointerOrientation = 'vertical';
  series.tooltip.background.cornerRadius = 20;
  series.tooltip.background.fillOpacity = 0.5;
  series.tooltip.label.padding(12, 12, 12, 12);
};

function generateChartData() {
  var chartData = [];
  var firstDate = new Date();
  firstDate.setDate(firstDate.getDate() - 1000);
  var visits = 1200;
  for (var i = 0; i < 500; i++) {
    // we create date objects here. In your data, you can have date strings
    // and then set format of your dates using chart.dataDateFormat property,
    // however when possible, use date objects, as this will speed up chart rendering.
    var newDate = new Date(firstDate);
    newDate.setDate(newDate.getDate() + i);

    visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

    chartData.push({
      date: newDate,
      visits: visits,
    });
  }
  return chartData;
}

function generateChartData1() {
  var chartData = [];
  for (var i in result.data) {
    console.log(result.data[i]);
    chartData.push({
      date: new Date(result.data[i].fecha_registro),
      visits: result.data[i].dato1,
    });
  }
  return chartData;
}

function generateChartData2() {
  var chartData = [];
  for (var i in result.data) {
    console.log(result.data[i]);
    chartData.push({
      date: new Date(result.data[i].fecha_registro),
      visits: result.data[i].dato2,
    });
  }
  return chartData;
}

//chart.exporting.menu = new am4core.ExportMenu();

const usuariosController = require('../controllers/usuarios');
const dataController = require('../controllers/data');

module.exports = function (io) {
  var numUsers = 0;

  io.on('connection', (socket) => {
    console.log('conection');
    socket.emit('connect', { data: 'WAA' });
    var addedUser = false;
    socket.on('add user', (username) => {
      console.log(username);
      if (addedUser) {
        console.log('nuevo usuario');
        return;
      }

      socket.username = username;
      ++numUsers;
      //addedUser = true;

      socket.emit('login', {
        numUsers: numUsers,
      });

      socket.broadcast.emit('user joined', {
        username: socket.username,
        usernameId: socket.id,
        numUsers: numUsers,
      });

      usuariosController.list().then(
        (success) => {
          socket.emit('listUser', {
            data: success,
          });
        },
        (error) => {
          console.log(error);
        }
      );
    });

    socket.on('new message', (data) => {
      if (data.idsocket != '') {
        io.to(data.idsocket).emit('new message', {
          username: socket.username,
          usernameId: socket.id,
          message: data.message,
        });
      } else {
        socket.broadcast.emit('new message', {
          username: socket.username,
          usernameId: socket.id,
          message: data.message,
        });
      }
    });

    socket.on('datos', (data) => {
      console.log('DATOS');
      const objeto_id = data.objeto_id;
      const dato1 = data.dato1;
      const dato2 = data.dato2;
      const fecha_registro = new Date();
      dataController.store(objeto_id, dato1, dato2, fecha_registro).then(
        (success) => {},
        (error) => {}
      );
    });
  });
};

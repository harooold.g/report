const { times } = require('lodash');
const mongoose = require('mongoose');

const data = new mongoose.Schema({
  objeto_id: { type: String, required: true },
  dato1: { type: String, required: true },
  dato2: { type: String, required: true },
  hora_registro: { type: String, default: new Date().toString() },
  fecha_registro: { type: Date, default: new Date() },
});

module.exports = mongoose.model('data', data);

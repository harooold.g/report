const mongoose = require('mongoose');

const objeto = new mongoose.Schema({
  hub_id: { type: String, required: true },
  nombre: { type: String, required: true },
  descripcion: { type: String, required: true },
  fecha_registro: { type: Date, default: new Date() },
});

module.exports = mongoose.model('objeto', objeto);

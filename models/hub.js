const mongoose = require('mongoose');

const hub = new mongoose.Schema({
  nombre: { type: String, required: true },
  empresa: { type: String, required: true },
  descripcion: { type: String, required: true },
  fecha_registro: { type: Date, default: new Date() },
});

module.exports = mongoose.model('hub', hub);

const { times } = require('lodash');
const mongoose = require('mongoose');

const config = new mongoose.Schema({
  correo1: { type: String, required: true },
  correo2: { type: String, required: false },
  correo3: { type: String, required: false },
  asunto: { type: String, required: true },
  contenido: { type: String, required: true },
});

module.exports = mongoose.model('config', config);

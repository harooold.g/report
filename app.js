require('dotenv').config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var cors = require('cors');

const mongoose = require('mongoose');
//mongodb://127.0.0.1/opcreporte
//mongodb+srv://hcohen:harold@opcdb.y93ce.mongodb.net/opcreporte?retryWrites=true&w=majority
mongoose
  .connect(
    'mongodb+srv://hcohen:harold@opcdb.y93ce.mongodb.net/opcreporte?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log('Mongo Db Connected'))
  .catch((err) => {
    console.log(Error, err.message);
  });

global.puppeteer = require('puppeteer');
global.nodemailer = require('nodemailer');
global.excel = require('exceljs');
global.refreshTokens = [];
global.utils = require('./helpers/utils');
global.PDFDocument = require('pdfkit');
global.fs = require('fs');

var authRouter = require('./routes/auth');
var refreshRouter = require('./routes/refresh');
var usuariosRouter = require('./routes/usuarios');
var hubRouter = require('./routes/hub');
var objetoRouter = require('./routes/objeto');
var dataRouter = require('./routes/data');
var reportRouter = require('./routes/report');
var configRouter = require('./routes/config');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(
  cors({
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
  })
);

app.set('view engine', 'ejs');
app.use(
  express.static('public', {
    setHeaders: (res, filepath) =>
      res.set(
        'Content-Disposition',
        `attachment; filename="pdf-express-${path.basename(filepath)}"`
      ),
  })
);

app.use('/auth', authRouter);

app.use('/api', utils.authenticateToken);
app.use('/refresh', refreshRouter);

app.use('/usuarios', usuariosRouter);
app.use('/hubs', hubRouter);
app.use('/objeto', objetoRouter);
app.use('/data', dataRouter);
app.use('/report', reportRouter);
app.use('/config', configRouter);

app.use('/api/usuarios', usuariosRouter);
app.use('/api/hub', hubRouter);
app.use('/api/objeto', objetoRouter);
app.use('/api/data', dataRouter);
app.use('/api/config', configRouter);

module.exports = app;
